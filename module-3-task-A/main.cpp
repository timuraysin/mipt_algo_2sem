#include <iostream>
#include <vector>

long long Prim(int numVertices, int numEdges, std::vector<std::vector<long long>>& graph) {    
    long long res = 0;
    std::vector<bool> processedVertices(numVertices + 1, false);
    std::vector<long long> distance(numVertices + 1, 2000000000000000);
    distance[1] = 0;

    for (int i = 1; i <= numVertices; ++i) {
        int currentVertice = -1;
        for (int j = 1; j <= numVertices; ++j) {
            if (!processedVertices[j] && (currentVertice == -1 || distance[j] < distance[currentVertice])) {
                currentVertice = j;
            }            
        }

        res += distance[currentVertice];
        processedVertices[currentVertice] = true;
        
        for (int to = 1; to <= numVertices; ++to) {
            if (graph[currentVertice][to] < distance[to]) {
                distance[to] = graph[currentVertice][to];
            }
        }
    }

    return res;
}

int main() {
    int numVertices = 0;
    int numEdges = 0;

    std::cin >> numVertices >> numEdges;

    std::vector<std::vector<long long>> graph(numVertices + 1, 
        std::vector<long long>(numVertices + 1, 2000000000000000));

    for (int i = 0; i < numEdges; ++i) {
        int from;
        int to;
        long long weight;

        std::cin >> from >> to >> weight;

        graph[from][to] = weight;
        graph[to][from] = weight;
    }

    std::cout << Prim(numVertices, numEdges, graph) << std::endl;

    return 0;
}