#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/Prim.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    std::vector<std::vector<long long>> graph(4, std::vector<long long>(4, 2000000000000000));
    graph[1][2] = 1;
    graph[2][3] = 1;    

    REQUIRE(Prim(3, 2, graph) == 2);    
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    std::vector<std::vector<long long>> graph(4, std::vector<long long>(4, 2000000000000000));
    graph[1][2] = 1;
    graph[1][3] = 1;
    graph[2][3] = 2;    

    REQUIRE(Prim(3, 3, graph) == 2);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    std::vector<std::vector<long long>> graph(5, std::vector<long long>(5, 2000000000000000));
    graph[1][2] = 3;
    graph[1][4] = 5;
    graph[2][3] = 3;
    graph[3][4] = 1;

    REQUIRE(Prim(4, 4, graph) == 7);
}