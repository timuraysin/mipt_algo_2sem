#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/solutionD.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    int rows = 2;
    int cols = 3;
    int aCost = 3;
    int bCost = 2;

    std::vector<std::vector<char>> field = {
        {'.', '*', '*'},
        {'.', '*', '.'}
    };

    REQUIRE(solve(rows, cols, aCost, bCost, field) == 5);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    int rows = 2;
    int cols = 3;
    int aCost = 4;
    int bCost = 2;

    std::vector<std::vector<char>> field {
        {'.', '*', '*'},
        {'.', '*', '*'}
    };

    REQUIRE(solve(rows, cols, aCost, bCost, field) == 8);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    int rows = 2;
    int cols = 3;
    int aCost = 3;
    int bCost = 2;

    std::vector<std::vector<char>> field = {
        {'.', '*', '.'},
        {'.', '*', '.'}
    };

    REQUIRE(solve(rows, cols, aCost, bCost, field) == 3);
}