#include <iostream>
#include <map>
#include <vector>

int solveEasy(int rows, int cols, int cost, std::vector<std::vector<char>>& field) {
    int result = 0;

    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (field[i][j] == '*') {
                result += cost;
            }
        }
    }

    return result;
}

std::vector<std::vector<int>> createGraph(int rows, int cols, std::vector<std::vector<char>>& field) {
    int numTiles = 0;
    std::vector<std::vector<int>> numbers(rows, std::vector<int>(cols, -1));

    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (field[i][j] == '*') {
                numbers[i][j] = numTiles;
                numTiles++;
            }
        }
    }

    std::vector<std::vector<int>> graph = std::vector<std::vector<int>>(numTiles);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (field[i][j] != '*') {
                continue;
            }

            int current = numbers[i][j];
            if ((i > 0) && numbers[i - 1][j] != -1) {
                graph[current].push_back(numbers[i - 1][j]);
            }

            if ((j > 0) && numbers[i][j - 1] != -1) {
                graph[current].push_back(numbers[i][j - 1]);
            }

            if ((i < rows - 1) && numbers[i + 1][j] != -1) {
                graph[current].push_back(numbers[i + 1][j]);
            }

            if ((j < cols - 1) && numbers[i][j + 1] != -1) {
                graph[current].push_back(numbers[i][j + 1]);
            }
        }
    }

    return graph;
}

bool algoKuhn(int vertice, std::vector<bool>& processedVertices, std::vector<int>& matching,
              std::vector<std::vector<int>>& graph) {
    if (processedVertices[vertice]) {
        return false;
    }

    processedVertices[vertice] = true;
    for (int i = 0; i < graph[vertice].size(); ++i) {
        int to = graph[vertice][i];
        if (matching[to] == -1 || algoKuhn(matching[to], processedVertices, matching, graph)) {
            matching[to] = vertice;
            return true;
        }
    }

    return false;
}

int solve(int rows, int cols, int aCost, int bCost, std::vector<std::vector<char>>& field) {
    if (bCost * 2 <= aCost) {
        return solveEasy(rows, cols, bCost, field);
    }

    std::vector<std::vector<int>> graph = createGraph(rows, cols, field);

    int size = static_cast<int>(graph.size());
    std::vector<bool> used(size, false);
    std::vector<int> matching(size, -1);

    int n = 0;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            used[j] = false;
        }
        algoKuhn(i, used, matching, graph);
    }

    int result = 0;

    for (int i = 0; i < size; ++i) {
        if (matching[i] != -1) {
            n += 1;
        }
    }

    n /= 2;

    return n * aCost + (size - 2 * n) * bCost;
}

int main() {
    int rows = 0;
    int cols = 0;
    int aCost = 0;
    int bCost = 0;

    std::cin >> rows >> cols >> aCost >> bCost;

    std::vector<std::vector<char>> field(rows, std::vector<char>(cols, '.'));

    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            std::cin >> field[i][j];
        }
    }

    std::cout << solve(rows, cols, aCost, bCost, field) << std::endl;

    return 0;
}