#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/solutionB.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    int numbers = 6;
    int queriesNumber = 8;
    std::vector<long long> array = {1, 0, 0, 0, 1, 0};
    long long capacity = 5;
    std::vector<std::vector<int>> queries = { 
        {0, 2, 1},
        {1, 4, 2},
        {2, 5, 1},
        {1, 3, 3},
        {3, 5, 2},
        {0, 4, 1},
        {0, 1, 3},
        {1, 3, 2}
     };

    std::vector<long long> res = solve(numbers, array, queriesNumber, queries, capacity);
    REQUIRE(res[0] == 3);
    REQUIRE(res[1] == 5);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    int numbers = 6;
    int queriesNumber = 2;
    std::vector<long long> array = {1, 0, 0, 0, 1, 0};
    long long capacity = 5;
    std::vector<std::vector<int>> queries = { 
        {0, 2, 1},
        {1, 4, 2}
     };

    std::vector<long long> res = solve(numbers, array, queriesNumber, queries, capacity);    
    REQUIRE(res.size() == 0);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    int numbers = 6;
    int queriesNumber = 3;
    std::vector<long long> array = {1, 0, 0, 0, 1, 0};
    long long capacity = 5;
    std::vector<std::vector<int>> queries = { 
        {0, 2, 1},
        {1, 4, 2},
        {2, 5, 1}
     };

    std::vector<long long> res = solve(numbers, array, queriesNumber, queries, capacity);
    REQUIRE(res.size() == 0);    
}