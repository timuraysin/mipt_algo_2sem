#include <iostream>
#include <vector>

void push(int v, int vl, int vr, std::vector<long long>& tree, std::vector<long long>& add) {
    if (vl != vr) {
        add[2 * v + 1] += add[v];
        add[2 * v + 2] += add[v];
    }
    tree[v] += add[v];
    add[v] = 0;
}

void build(int v, int vl, int vr, std::vector<long long>& tree, std::vector<long long>& a) {
    if (vl == vr) {
        tree[v] = a[vl];
    } else {
        int vm = vl + (vr - vl) / 2;
        build(2 * v + 1, vl, vm, tree, a);
        build(2 * v + 2, vm + 1, vr, tree, a);
        tree[v] = std::max(tree[2 * v + 1], tree[2 * v + 2]);
    }
}

long long query(int v, int vl, int vr, int l, int r, std::vector<long long>& tree, std::vector<long long>& add) {
    push(v, vl, vr, tree, add);
    if (r < vl || vr < l) {
        return 0;
    }
    if (l <= vl && vr <= r) {
        return tree[v];
    }
    int vm = vl + (vr - vl) / 2;
    long long ql = query(2 * v + 1, vl, vm, l, r, tree, add);
    long long qr = query(2 * v + 2, vm + 1, vr, l, r, tree, add);
    return std::max(ql, qr);
}

void modify(int v, int vl, int vr, int l, int r, int val, std::vector<long long>& tree, std::vector<long long>& add) {
    push(v, vl, vr, tree, add);

    if (r < vl || vr < l) {
        return;
    }

    if (l <= vl && vr <= r) {
        add[v] += val;
        push(v, vl, vr, tree, add);
        return;
    }

    int vm = vl + (vr - vl) / 2;
    modify(2 * v + 1, vl, vm, l, r, val, tree, add);
    modify(2 * v + 2, vm + 1, vr, l, r, val, tree, add);
    tree[v] = std::max(tree[2 * v + 1], tree[2 * v + 2]);
}

std::vector<long long> solve(int stations, std::vector<long long>& a, int queriesNumber, std::vector<std::vector<int>>& queries,
    long long capacity) {
    std::vector<long long> tree = std::vector<long long>(4 * stations, 0);
    std::vector<long long> add = std::vector<long long>(4 * stations, 0);

    build(0, 0, stations - 1, tree, a);

    std::vector<long long> res;

    for (int q = 0; q < queriesNumber; ++q) {
        int left = queries[q][0];
        int right = queries[q][1];
        int tickets = queries[q][2];

        long long s = query(0, 0, stations - 1, left, right - 1, tree, add);
        if (s + tickets <= capacity) {
            a[left] += tickets;
            a[right] -= tickets;
            modify(0, 0, stations - 1, left, right - 1, tickets, tree, add);
        } else {
            res.push_back(q);
        }
    }

    return res;
}
