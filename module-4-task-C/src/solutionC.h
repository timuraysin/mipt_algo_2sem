#include <iostream>
#include <vector>

struct Color {    
    Color(int r, int g, int b);

    int sum();

    int r;
    int g;
    int b;
};

Color::Color(int r, int g, int b) :
    r(r),
    g(g),
    b(b)
{
}

int Color::sum() {
    return r + g + b;
}

bool operator<(const Color& c1, const Color& c2) {
    return (c1.r + c1.g + c1.b) < (c2.r + c2.g + c2.b);
}

bool operator==(const Color& c1, const Color& c2) {
    return (c1.r == c2.r) && (c2.g == c1.g) && (c2.b == c1.b);
}

bool operator!=(const Color& c1, const Color& c2) {
    return (c1 == c2);
}

void build(int v, int vl, int vr, std::vector<Color>& tree, std::vector<Color>& a) {
    if (vl == vr) {
        tree[v] = a[vl];
    } else {
        int vm = vl + (vr - vl) / 2;
        build(2 * v + 1, vl, vm, tree, a);
        build(2 * v + 2, vm + 1, vr, tree, a);
        tree[v] = std::min(tree[2 * v + 1], tree[2 * v + 2]);
    }
}

void push(int v, int vl, int vr, std::vector<Color>& tree, std::vector<int>& propagate) {
    if (propagate[v] && vl != vr) {
        tree[2 * v + 1] = tree[v];
        tree[2 * v + 2] = tree[v];
        propagate[2 * v + 1] = 1;
        propagate[2 * v + 2] = 1;
        propagate[v] = 0;
    }
}

Color query(int v, int vl, int vr, int l, int r, std::vector<Color>& tree, std::vector<int>& propagate) {
    if (l > r || r < vl || vr < l) {
        return Color(20000000, 20000000, 20000000);
    }
    push(v, vl, vr, tree, propagate);
    if (l <= vl && vr <= r) {
        return tree[v];
    }
    int vm = vl + (vr - vl) / 2;
    Color ql = query(2 * v + 1, vl, vm, l, r, tree, propagate);
    Color qr = query(2 * v + 2, vm + 1, vr, l, r, tree, propagate);
    return std::min(ql, qr);
}

void update(int v, int vl, int vr, int l, int r, Color color, std::vector<Color>& tree, 
    std::vector<int>& propagate) {
    push(v, vl, vr, tree, propagate);

    if (l > r || r < vl || vr < l) {
        return;
    }

    if (l <= vl && vr <= r) {
        tree[v] = color;
        propagate[v] = 1;
        push(v, vl, vr, tree, propagate);
    }

    else {
        int vm = vl + (vr - vl) / 2;
        update(v * 2 + 1, vl, vm, l, r, color, tree, propagate);
        update(v * 2 + 2, vm + 1, vr, l, r, color, tree, propagate);
        tree[v] = std::min(tree[2*v+1], tree[2*v+2]);
    }
}

std::vector<int> solve(int size, std::vector<Color>& array, int queriesNumber, 
    std::vector<std::vector<int>>& queries) {
    std::vector<Color> tree = std::vector<Color>(4 * size, Color(0, 0, 0));
    std::vector<int> propagate = std::vector<int>(4 * size, 0);

    build(0, 0, size - 1, tree, array);

    std::vector<int> res;

    for (int q = 0; q < queriesNumber; ++q) {
        int left = queries[q][0];
        int right = queries[q][1];

        Color newColor = Color(queries[q][2], queries[q][3], queries[q][4]);

        update(0, 0, size - 1, left, right, newColor, tree, propagate);

        left = queries[q][5];
        right = queries[q][6]; 

        Color minimum = query(0, 0, size - 1, left, right, tree, propagate);

        res.push_back(minimum.sum());    
    }

    return res;
}
