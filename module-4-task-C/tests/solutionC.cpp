#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/solutionC.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    int size = 5;
    std::vector<Color> array = {
        Color(7, 40, 3),
        Color(54, 90, 255),
        Color(44, 230, 8),
        Color(33, 57, 132),
        Color(17, 8, 5)
    };

    int queriesNumber = 2;
    std::vector<std::vector<int>> queries = {
        {0, 3, 100, 40, 41, 2, 4},
        {2, 4, 0, 200, 57, 1, 3}
    };

    std::vector<int> res = solve(size, array, queriesNumber, queries);

    REQUIRE(res[0] == 30);
    REQUIRE(res[1] == 181);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    int size = 5;
    std::vector<Color> array = {
        Color(7, 40, 3),
        Color(54, 90, 255),
        Color(44, 230, 8),
        Color(33, 57, 132),
        Color(17, 8, 5)
    };

    int queriesNumber = 1;
    std::vector<std::vector<int>> queries = {
        {0, 3, 100, 40, 41, 2, 4}
    };

    std::vector<int> res = solve(size, array, queriesNumber, queries);

    REQUIRE(res[0] == 30);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    int size = 5;
    std::vector<Color> array = {
        Color(7, 40, 3),
        Color(54, 90, 255),
        Color(44, 230, 8),
        Color(33, 57, 132),
        Color(17, 8, 5)
    };

    int queriesNumber = 0;
    std::vector<std::vector<int>> queries = {
        
    };

    std::vector<int> res = solve(size, array, queriesNumber, queries);

    REQUIRE(res.size() == 0);
}