#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/SolutionE.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    int numVertices = 3;
    int numEdges = 3;
    int startVertice = 1;
    int endVertice = 3;
    std::vector<std::pair<int, int>> edges = {
        {1, 2},
        {1, 3},
        {2, 3}
    };

    REQUIRE(solve(numVertices, numEdges, startVertice, endVertice, edges) == true);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    int numVertices = 3;
    int numEdges = 2;
    int startVertice = 1;
    int endVertice = 2;
    std::vector<std::pair<int ,int>> edges = {
        {1, 2},
        {3, 2}
    };

    REQUIRE(solve(numVertices, numEdges, startVertice, endVertice, edges) == false);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    int numVertices = 4;
    int numEdges = 3;
    int startVertice = 1;
    int endVertice = 4;
    std::vector<std::pair<int ,int>> edges = {
        {1, 3},
        {2, 3},
        {3, 4}
    };

    REQUIRE(solve(numVertices, numEdges, startVertice, endVertice, edges) == false);
}