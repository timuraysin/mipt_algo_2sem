#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

const int INF = 2000000000;

struct Edge {
    
public:
    Edge();    
    Edge(int t, long long f, long long c);
    Edge(const Edge& other);

	int to;
    long long f;
    long long c;
    int backIndex;
    int mark = 0;
};

Edge::Edge() {
    to = 0;
    f = 0;
    c = 0;
    backIndex = -1;
}

Edge::Edge(int t, long long f, long long c) :
    to(t),
    f(f),
    c(c),
	backIndex(-1)
{       
}

Edge::Edge(const Edge& other) {
	to = other.to;
    f = other.f;
    c = other.c;
    backIndex = other.backIndex;
    mark = other.mark;
}

int dfs(int vertice, int end, long long Cmin, std::vector<std::vector<Edge>>& graph,
        std::vector<bool>& visited) {
    if (vertice == end) {
        return Cmin;
    }

    visited[vertice] = true;
    for (int i = 0; i < graph[vertice].size(); ++i) {
        Edge e = graph[vertice][i];
        int to = e.to;
        if (!visited[to] && e.f < e.c) {
            long long delta = dfs(to, end, std::min(Cmin, e.c - e.f), graph, visited);
            if (delta > 0) {
                graph[vertice][i].f += delta;
                graph[to][graph[vertice][i].backIndex].f -= delta;
                return delta;
            }
        }
    }

    return 0;
}

void print(int vertice, int end, int numVertices, int &count, std::vector<std::vector<Edge>>& graph,
           std::vector<bool>& visited, std::vector<int>& path) {
    if (vertice == end) {
        count++;
        std::cout << std::endl;
        for (auto x : path) {
            if (x <= numVertices)
                std::cout << x << " ";
        }
        return;
    }

    visited[vertice] = true;
    for (int j = 0; j < graph[vertice].size(); ++j) {
        Edge e = graph[vertice][j];
        int to = e.to;
        if (count == 1) {
            return;
        }
        if (!visited[to] && e.f > 0 && e.mark != 1) {
            path.push_back(to);
            graph[vertice][j].mark = 1;
            print(to, end, numVertices, count, graph, visited, path);
            if (count == 1) return;
            path.pop_back();
            graph[vertice][j].mark = 0;
        }
    }
}

int main() {
    int numVertices = 0;
    int numEdges = 0;
    int startVertice = 0;
    int endVertice = 0;

    std::cin >> numVertices >> numEdges >> startVertice >> endVertice;

    std::vector<std::vector<Edge>> graph(numVertices + numEdges + 1);
    std::vector<std::vector<Edge>> graphCopy(numVertices + numEdges + 1);
    std::vector<bool> visited(numVertices + numEdges + 1, false);
    int additional = numVertices + 1;

    for (int i = 0; i < numEdges; ++i) {
        int from, to;

        std::cin >> from >> to;

        if (from == to) {
            continue;
        }

        graph[from].push_back(Edge(additional, 0, 1));
        graph[additional].push_back(Edge(from, 0, 0));

        graph[from].back().backIndex = graph[additional].size() - 1;
        graph[additional].back().backIndex = graph[from].size() - 1;

        graph[additional].push_back(Edge(to, 0, 1));
        graph[to].push_back(Edge(additional, 0, 0));

        graph[additional].back().backIndex = graph[to].size() - 1;
        graph[to].back().backIndex = graph[additional].size() - 1;


        additional++;
    }

    graphCopy = graph;

    int val = dfs(startVertice, endVertice, INF, graph, visited);
    int flow = 0;
    while (val > 0) {
        flow += val;

        if (flow >= 2) {
            break;
        }

        visited.assign(numEdges + numVertices + 1, false);
        val = dfs(startVertice, endVertice, INF, graph, visited);
    }

    for (int i = 1; i < numEdges + numVertices + 1; ++i) {
        for (int j = 0; j < graph[i].size(); ++j) {
            Edge e = graph[i][j];
            int to = e.to;

            int newFValue = e.f;
            int newBValue = graph[to][e.backIndex].f;

            int oldFValue = graphCopy[i][j].f;
            int oldBValue = graphCopy[to][e.backIndex].f;

            graph[i][j].f = -(newFValue - oldFValue);
            graph[to][e.backIndex].f = -(newBValue - oldBValue);

            graphCopy[i][j].f = 0;
            graphCopy[to][e.backIndex].f = 0;
        }
    }

    if (flow >= 2) {
        std::cout << "YES";
        visited.assign(numEdges + numVertices + 1, false);
        std::vector<int> path;
        path.push_back(startVertice);
        int count = 0;
        print(startVertice, endVertice, numVertices, count, graph, visited, path);
        count = 0;
        path = {startVertice};
        visited.assign(numEdges + numVertices + 1, false);
        print(startVertice, endVertice, numVertices, count, graph, visited, path);
        std::cout << std::endl;
    } else {
        std::cout << "NO" << std::endl;
    }

    return 0;
}