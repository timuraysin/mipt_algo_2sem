#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/solutionF.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    int numVertices = 4;    
    std::vector<std::vector<int>> graph = {
        {0, 1, 1, 1},
        {1, 0, 0, 1},
        {1, 0, 0, 1},
        {1, 1, 1, 0}
    };

    std::vector<std::vector<int>> res = solve(numVertices, graph);

    REQUIRE(res[0][0] == 3);
    REQUIRE(res[1][0] == 1);
    REQUIRE(res[1][1] == 2);
    REQUIRE(res[1][2] == 4);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    int numVertices = 3;    
    std::vector<std::vector<int>> graph = {
        {0, 1, 1},
        {1, 0, 0},
        {1, 0, 0}
    };

    std::vector<std::vector<int>> res = solve(numVertices, graph);

    REQUIRE(res[0][0] == 3);
    REQUIRE(res[1][0] == 1);
    REQUIRE(res[1][1] == 2);    
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    int numVertices = 4;
    std::vector<std::vector<int>> graph = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 1},
        {0, 0, 1, 0}
    };

    std::vector<std::vector<int>> res = solve(numVertices, graph);

    REQUIRE(res[0][0] == 3);
    REQUIRE(res[0][1] == 4);
    REQUIRE(res[1][0] == 1);
    REQUIRE(res[1][1] == 2);    
}