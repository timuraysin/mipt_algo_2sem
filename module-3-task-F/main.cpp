#include <algorithm>
#include <iostream>
#include <vector>

void solve(int numVertices, std::vector<std::vector<int>>& graph) {
    std::vector<std::vector<int>> sourceNumber(numVertices);
    for (int i = 0; i < numVertices; ++i) {
        sourceNumber[i].assign(1, i);
    }

    int bestCost = 2000000000;

    std::vector<bool> exists(numVertices, true);
    std::vector<int> weight(numVertices, 0);
    std::vector<bool> inCut(numVertices, false);
    std::vector<int> bestCut;

    for (int phase = 0; phase < numVertices - 1; ++phase) {
        inCut.assign(numVertices, false);
        weight.assign(numVertices, 0);
        for (int it = 0, prev; it < numVertices - phase; ++it) {
            int sel = -1;
            for (int i = 0; i < numVertices; ++i) {
                if (exists[i] && !inCut[i] && (sel == -1 || weight[i] > weight[sel])) {
                    sel = i;
                }
            }
            if (it == numVertices - phase - 1) {
                if (weight[sel] < bestCost) {
                    bestCost = weight[sel];
                    bestCut = sourceNumber[sel];
                }

                sourceNumber[prev].insert(sourceNumber[prev].end(), sourceNumber[sel].begin(), sourceNumber[sel].end());
                for (int i = 0; i < numVertices; ++i) {
                    graph[prev][i] = graph[i][prev] += graph[sel][i];
                }
                exists[sel] = false;
            }
            else {
                inCut[sel] = true;
                for (int i = 0; i < numVertices; ++i) {
                    weight[i] += graph[sel][i];
                }
                prev = sel;
            }
        }
    }

    for (auto v : bestCut) {
        std::cout << v + 1 << " ";
    }

    std::cout << std::endl;

    for (int i = 0; i < numVertices; ++i) {
        if (find(bestCut.begin(), bestCut.end(), i) == bestCut.end()) {
            std::cout << i + 1 << " ";
        }
    }

    std::cout << std::endl;
}

int main() {
    int numVertices = 0;
    std::cin >> numVertices;

    std::vector<std::vector<int>> graph(numVertices, std::vector<int>(numVertices, 0));

    char sym;
    for (int i = 0; i < numVertices; ++i) {
        for (int j = 0; j < numVertices; ++j) {
            std::cin >> sym;

            if (sym == '1') {
                graph[i][j] = 1;
            }
        }
    }

    solve(numVertices, graph);

    return 0;
}
