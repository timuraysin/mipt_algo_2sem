#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <vector>

const long long INF = 2000000000000000000;

long long solve(int numVertices, int numEdges, std::vector<std::vector<std::pair<int, long long>>>& graph,
    std::vector<long long>& weight) {
    long long res = 0;
    int startVertice = -1;
    for (int i = 1; i <= numVertices; ++i) {
        if (startVertice == -1 || weight[startVertice] > weight[i]) {
            startVertice = i;
        }
    }
    
    for (int i = 1; i <= numVertices; ++i) {
        if (i == startVertice) {
            continue;
        }

        graph[i].push_back({startVertice, weight[startVertice] + weight[i]});
        graph[startVertice].push_back({i, weight[startVertice] + weight[i]});
    }

    std::vector<long long> distance(numVertices + 1, INF);
    distance[startVertice] = 0;
    std::set<std::pair<long long, int>> queueToProcess;
    queueToProcess.insert({0, startVertice});
    std::vector<int> processed(numVertices + 1, 0);

    for (int i = 1; i <= numVertices; i++) {
        int currentVertice = queueToProcess.begin()->second;
        queueToProcess.erase(queueToProcess.begin());

        while (processed[currentVertice]) {
            currentVertice = queueToProcess.begin()->second;
            queueToProcess.erase(queueToProcess.begin());
        }

        processed[currentVertice] = 1;
        res += distance[currentVertice];

        for (size_t j = 0; j < graph[currentVertice].size(); ++j) {
            int to = graph[currentVertice][j].first;
            long long cost = graph[currentVertice][j].second;

            if (cost < distance[to]) {
                queueToProcess.erase({distance[to], to});
                distance[to] = cost;
                queueToProcess.insert({distance[to], to});
            }
        }
    }

    return res;
}

int main() {
    int numVertices = 0;
    int numEdges = 0;

    std::cin >> numVertices >> numEdges;

    std::vector<std::vector<std::pair<int, long long>>> graph(numVertices + 1);
    std::vector<long long> weight(numVertices + 1);

    for (int i = 1; i <= numVertices; ++i) {
        long long w;
        std::cin >> w;

        weight[i] = w;
    }

    for (int i = 0; i < numEdges; ++i) {
        int from;
        int to;
        long long weight;

        std::cin >> from >> to >> weight;

        graph[from].push_back({to, weight});
        graph[to].push_back({from, weight});
    }

    std::cout << solve(numVertices, numEdges, graph, weight) << std::endl;

    return 0;
}