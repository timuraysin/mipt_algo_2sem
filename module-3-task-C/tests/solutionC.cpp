#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/SolutionC.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    int numVertices = 3;
    int numEdges = 2;

    std::vector<std::vector<std::pair<int, long long>>> graph(numVertices + 1);
    std::vector<long long> weight(numVertices + 1);

    weight[1] = 1;
    weight[2] = 3;
    weight[3] = 3;

    graph[2].push_back({3, 5});
    graph[3].push_back({2, 5});
    graph[2].push_back({1, 1});
    graph[1].push_back({2, 1});

    REQUIRE(solve(numVertices, numEdges, graph, weight) == 5);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    int numVertices = 4;
    int numEdges = 0;

    std::vector<std::vector<std::pair<int, long long>>> graph(numVertices + 1);
    std::vector<long long> weight(numVertices + 1);

    weight[1] = 1;
    weight[2] = 3;
    weight[3] = 3;
    weight[4] = 7;

    REQUIRE(solve(numVertices, numEdges, graph, weight) == 16);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    int numVertices = 5;
    int numEdges = 4;

    std::vector<std::vector<std::pair<int, long long>>> graph(numVertices + 1);
    std::vector<long long> weight(numVertices + 1);

    weight[1] = 1;
    weight[2] = 2;
    weight[3] = 3;
    weight[4] = 4;
    weight[5] = 5;

    graph[1].push_back({2, 8});
    graph[2].push_back({1, 8});

    graph[1].push_back({3, 10});
    graph[3].push_back({1, 10});

    graph[1].push_back({4, 7});
    graph[4].push_back({1, 7});

    graph[1].push_back({5, 15});
    graph[5].push_back({1, 15});

    REQUIRE(solve(numVertices, numEdges, graph, weight) == 18);
}