/*
    Автор: Айсин Тимур Б05-927
    Описание:
    
    Решение задачи B.

    Решение:
        Решением этой задачи будет список из вершин после применения топологической сортировки, или
        же вывод NO, если существует цикл. Топологическую сортировку и поиск цикла осуществляем
        одновременно с помощью поиска в глубину.
    Память: O(n+m), Время: O(n+m)
*/
#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <vector>

enum TVertexColorType {
    VCT_WHITE,
    VCT_GRAY,
    VCT_BLACK
};

// возвращает true, если есть цикл, false - иначе
bool TopSort( int currentVertice, const std::vector<std::vector<int>>& graph, 
    std::vector<TVertexColorType>& verticesColor, std::stack<int>& sortedVertices) 
{
    verticesColor[currentVertice] = VCT_GRAY; // красим в серый цвет

    for( auto reachableVertice : graph[currentVertice] ) {
        if( verticesColor[reachableVertice] == VCT_GRAY ) { // если существует путь в серую вершину из серой - цикл
            return true;
        } else if( verticesColor[reachableVertice] == VCT_WHITE && 
            TopSort( reachableVertice, graph, verticesColor, sortedVertices ) ) {          
            // если поиск из этой вершины нашел цикл, то возвращаем true
            return true;            
        }
    }

    verticesColor[currentVertice] = VCT_BLACK; // красим в черный
    // добавляем эту вершину в стек, так как все вершины, куда идет ребро из данной, уже добавлены в стек
    sortedVertices.push( currentVertice ); 
    return false;
}

std::string PrintSortedVertices( std::stack<int>& sortedVertices )
{
    std::string result = "YES\n";

    while( !sortedVertices.empty() ) {
        result += std::to_string( sortedVertices.top() ) + " ";
        sortedVertices.pop();
    }

    result += "\n";
    return result;
}

int main() 
{
    int numVertices = 0;
    int numEdges = 0;
    std::cin >> numVertices >> numEdges;

    std::vector<std::vector<int>> graph( numVertices );
    std::vector<TVertexColorType> verticesColor( numVertices, VCT_WHITE ); // массив, в котором содержатся цвета вершин, 0 - белый, 1 - серый, 2 - черный

    for( int i = 0; i < numEdges; ++i ) {
        int v1 = 0;
        int v2 = 0;

        std::cin >> v1 >> v2;
        
        graph[v1].push_back(v2);                
    }    

    std::stack<int> sortedVertices; // стек, в котором хранятся вершины после топологической сортировки

    bool verdict = true; // вердикт программы

    for( int i = 0; i < numVertices; ++i ) {
        if( verticesColor[i] == VCT_WHITE ) {
            if( TopSort( i, graph, verticesColor, sortedVertices ) ) {
                verdict = false;
                break;
            }
        }
    }

    std::cout << (verdict ? PrintSortedVertices( sortedVertices ) : "NO") << std::endl;

    return 0;
}