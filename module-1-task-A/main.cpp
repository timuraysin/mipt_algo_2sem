// Автор: Айсин Тимур Б05-927

/*
    Описание: Решение задачи A из контеста.
    Решение:

    Нам необходимо посчитать минимальное кол-во ребер, по которым пройдут Леон и Матильда, отправляясь за молоком.
    Заведем массивы d1, d2, d3, каждый из них содержит кратчайшие расстояния до всех вершин от Леона, Матильды, и
    магазина
    с молоком соответственно. Чтобы посчитать значения в массивах, воспользуемся алгоритмом поиска в ширину.
    Пусть ответ, который мы пытаемся улучшить, будет суммой расстояний от Леона и Матильды до магазина с молоком.
    Затем переберем город, в котором они могут встретиться и от которого пойдут вместе по кратчайшему пути до 
    магазина.
    Минимум из всех этих значений и будет ответом задачи.

    Память: O(n), Время: O(n+m)
*/
#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>

enum TEntityNameType {
    ENT_LEON,
    ENT_MATILDA,
    ENT_MILK
};

void CalculateDistance( int startVertice, const std::vector<std::vector<int>>& graph, 
    std::vector<std::vector<int>>& distances, TEntityNameType ent ) 
{
    std::queue<int> queue;
    queue.push( startVertice );

    while( !queue.empty() ) {
        int currentVertice = queue.front();
        queue.pop();

        for( auto reachableVertice : graph[currentVertice] ) {
            if( distances[ent][reachableVertice] == -1 ) {
                distances[ent][reachableVertice] = distances[ent][reachableVertice] + 1;
                queue.push( reachableVertice );
            }
        }
    }
}

int main() 
{
    int numVertices = 0;
    int numEdges = 0;

    // leon, mat, milk - номера вершин персонажей в графе
    int leon = 0;
    int mat = 0;
    int milk = 0; 

    std::cin >> numVertices >> numEdges >> leon >> mat >> milk;

    std::vector<std::vector<int>> graph( numVertices + 1 );

    // заполняем список смежности значениями
    for( int i = 0; i < numEdges; ++i ) {
        int v1;
        int v2;
        std::cin >> v1 >> v2;

        graph[v1].push_back( v2 );
        graph[v2].push_back( v1 );
    }    

    std::vector<std::vector<int>> distances(3, std::vector<int>( numVertices + 1, -1 ));

    // Инициализация
    distances[ENT_LEON][leon] = 0;
    distances[ENT_MATILDA][mat] = 0;
    distances[ENT_MILK][milk] = 0;    

    // Считаем расстояния от Леона до вершин
    CalculateDistance( leon, graph, distances, ENT_LEON );

    // Считаем расстояния от Матильды до вершин
    CalculateDistance( mat, graph, distances, ENT_MATILDA );

    // Считаем расстояния от магазина с молоком до вершин
    CalculateDistance( milk, graph, distances, ENT_MILK );

    int ans = distances[ENT_LEON][milk] + distances[ENT_MATILDA][milk];

    // Перебираем город, в котором они встретятся и пойдут по одним дорогам
    for( int i = 1; i <= numVertices; i++ ) {
        ans = std::min( ans, distances[ENT_MILK][i] + distances[ENT_LEON][i] + distances[ENT_MATILDA][i] );
    }

    std::cout << ans << std::endl;

    return 0;
}