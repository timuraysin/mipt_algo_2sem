#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/solutionA.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    int numbers = 10;
    int queriesNumber = 3;
    std::vector<long long> array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    std::vector<std::pair<int,int>> queries = {
        {1, 2},
        {1, 10},
        {2, 7}
    };

    std::vector<long long> res = solve(numbers, queriesNumber, array, queries);
    
    REQUIRE(res[0] == 2);
    REQUIRE(res[1] == 2);
    REQUIRE(res[2] == 3);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    int numbers = 10;
    int queriesNumber = 2;
    std::vector<long long> array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    std::vector<std::pair<int,int>> queries = {
        {1, 2},
        {1, 10}
    };

    std::vector<long long> res = solve(numbers, queriesNumber, array, queries);

    REQUIRE(res[0] == 2);
    REQUIRE(res[1] == 2);
    REQUIRE(res.size() == 2);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    int numbers = 10;
    int queriesNumber = 0;
    std::vector<long long> array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    std::vector<std::pair<int,int>> queries = {};

    std::vector<long long> res = solve(numbers, queriesNumber, array, queries);

    REQUIRE(res.size() == 0);
}