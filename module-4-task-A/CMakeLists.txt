cmake_minimum_required(VERSION 3.5)

project("testExecutable")

# AddressSanitize
set(CMAKE_C_FLAGS_ASAN
    "-fsanitize=address -fno-optimize-sibling-calls -fsanitize-address-use-after-scope -fno-omit-frame-pointer -g -O1"
    CACHE STRING "Flags used by the C compiler during AddressSanitizer builds."
    FORCE)
set(CMAKE_CXX_FLAGS_ASAN
    "-fsanitize=address -fno-optimize-sibling-calls -fsanitize-address-use-after-scope -fno-omit-frame-pointer -g -O1"
    CACHE STRING "Flags used by the C++ compiler during AddressSanitizer builds."
    FORCE)

find_package(Catch2 REQUIRED)
add_executable(testExecutable tests/tests.cpp)
target_link_libraries(testExecutable Catch2::Catch2)

target_link_libraries(testExecutable asan)
include(Catch)
catch_discover_tests(testExecutable)