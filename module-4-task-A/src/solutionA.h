#include <algorithm>
#include <iostream>
#include <vector>

long long max2Power(long long len) {
    if (len == 1) {
        return 0;
    }

    return max2Power(len / 2) + 1;
}

std::vector<long long> solve(int numbers, int queriesNumber, std::vector<long long>& array,
    std::vector<std::pair<int, int>>& queries) {
    std::vector<int> logs(numbers + 1, 0);
    for (int i = 1; i <= numbers; ++i) {        
        logs[i] = max2Power(i);
    }

    std::vector<std::vector<std::pair<long long, long long>>> sparseTable(numbers + 1,
        std::vector<std::pair<long long, long long>>(
        logs[numbers] + 1));

    for (int i = 1; i <= numbers; ++i) {
        sparseTable[i][0] = {i, -1};
    }

    for (int j = 1; j <= logs[numbers]; ++j) {
        for (int i = 1; i + (1 << j) <= numbers + 1; ++i) {
            int ind = i + (1 << (j - 1));

            std::pair<long, long> p1 = sparseTable[i][j - 1];
            std::pair<long, long> p2 = sparseTable[ind][j - 1];

            if (j == 1) {
                if (array[p1.first] > array[p2.first]) {
                    sparseTable[i][j] = {p2.first, p1.first};
                } else {
                    sparseTable[i][j] = {p1.first, p2.first};
                }
            } else if (p1.first != p2.first) {
                if (array[p1.first] > array[p2.first]) {
                    if (array[p2.second] > array[p1.first]) {
                        sparseTable[i][j] = {p2.first, p1.first};
                    } else {
                        sparseTable[i][j] = {p2.first, p2.second};
                    }
                } else if (array[p1.first] < array[p2.first]) {
                    if (array[p1.second] > array[p2.first]) {
                        sparseTable[i][j] = {p1.first, p2.first};
                    } else {
                        sparseTable[i][j] = {p1.first, p1.second};
                    }
                } else {
                    sparseTable[i][j] = {p1.first, p2.first};
                }
            } else {
                if (array[p2.second] > array[p1.second]) {
                    sparseTable[i][j] = {p1.first, p1.second};
                } else {
                    sparseTable[i][j] = {p1.first, p2.second};
                }
            }
        }
    }

    std::vector<long long> res;

    for (int q = 0; q < queriesNumber; ++q) {
        int l, r;
        l = queries[q].first;
        r = queries[q].second;

        int len = r - l + 1;
        int j = logs[len];

        int ind = r - (1 << j) + 1;

        std::pair<long long, long long> p1 = sparseTable[l][j];
        std::pair<long long, long long> p2 = sparseTable[r - (1 << j) + 1][j];

        if (p1.first == p2.first) {
            if (array[p1.second] > array[p2.second]) {
                res.push_back(array[p2.second]);
            } else {
                res.push_back(array[p1.second]);
            }
        } else {
            if (array[p1.first] > array[p2.first]) {
                if (array[p2.second] > array[p1.first]) {
                    res.push_back(array[p1.first]);
                } else {
                    res.push_back(array[p2.second]);
                }
            } else if (array[p1.first] < array[p2.first]) {
                if (array[p1.second] > array[p2.first]) {
                    res.push_back(array[p2.first]);
                } else {
                    res.push_back(array[p1.second]);
                }
            } else {
                res.push_back(array[p1.first]);
            }
        }
    }

    return res;
}