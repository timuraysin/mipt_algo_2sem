#include <set>
#include <vector>
#include <string>

// converts value to string with specified length
std::string convert(long long length, long long val) {    
    long long temp = val;
    std::string result = "";

    while (temp != 0) {
        result = (temp % 2 == 0 ? "0" : "1") + result;
        temp /= 2;
    }

    if (result.size() < length) {
        int c = result.size();
        for (int k = 1; k <= length - c; k++)
        {
            result = "0" + result;
        }
    }
    return result;
}

void SolutionD(long long numberOfVertices, std::vector<std::vector<long long>>& graphInNumbers,
    std::vector<std::vector<std::string>>& graphInStrings, std::vector<std::string>& result) {
    // оптимизированный алгоритм Флойда-Уоршелла
    for (int k = 0; k < numberOfVertices; ++k) {
        for (int i = 0; i < numberOfVertices; i++) {
            long long number = graphInNumbers[i][k / 32];
            long long stringSize = graphInStrings[i][k / 32].size() - 1;
            int index = k % 32;
            // если бит равен 1, то ребро существует и пытаемся обновить ответ
            if (number >> (stringSize - index) & 1 == 1) {
                for (int j = 0; j <= numberOfVertices / 32 + 1; j++) {                    
                    graphInNumbers[i][j] = graphInNumbers[i][j] | graphInNumbers[k][j];                 
                }
            }
        }
    }

    for (int i = 0; i < numberOfVertices; ++i) {
        std::string temp = "";
        for (int j = 0; j < numberOfVertices / 32 + 1; ++j) {
            temp += convert(graphInStrings[i][j].size(), graphInNumbers[i][j]);
        }
        result[i] = temp;
    }
}