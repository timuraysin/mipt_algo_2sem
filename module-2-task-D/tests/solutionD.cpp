#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/SolutionD.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    std::vector<std::string> res;
    int vertices = 2;
    std::vector<std::vector<long long>> graphNumbers = { {2}, {3} };
    std::vector<std::vector<std::string>> graphStrings = { {"00"}, {"01"} };

    SolutionD(vertices, graphNumbers, graphStrings, res);

    REQUIRE(res[0] == "00");
    REQUIRE(res[1] == "01");
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    std::vector<std::string> res;
    int vertices = 3;
    std::vector<std::vector<long long>> graphNumbers = { {2}, {1}, {0} };
    std::vector<std::vector<std::string>> graphStrings = { {"010"}, {"001"}, {"000"} };

    SolutionD(vertices, graphNumbers, graphStrings, res);

    REQUIRE(res[0] == "011");
    REQUIRE(res[1] == "001");
    REQUIRE(res[2] == "000");
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    std::vector<std::string> res;
    int vertices = 1;
    std::vector<std::vector<long long>> graphNumbers = { {0} };
    std::vector<std::vector<std::string>> graphStrings = { {"0"} };

    SolutionD(vertices, graphNumbers, graphStrings, res);

    REQUIRE(res[0] ==  "0" );
}