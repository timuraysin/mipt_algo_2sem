#include <iostream>
#include <set>
#include <vector>

int main() {
    long long numberOfVertices = 0;    
    long long numberOfEdges = 0;
    long long numberOfTravels = 0;
    long long startVertice = 0;
    long long endVertice = 0;

    std::cin >> numberOfVertices >> numberOfEdges >> numberOfTravels >> startVertice >> endVertice;

    std::vector<std::vector<std::pair<long long, long long>>> edges(numberOfVertices + 1);

    for (int i = 0; i < numberOfEdges; ++i) {
        long long from = 0;
        long long to = 0;
        long long cost = 0;
        std::cin >> from >> to >> cost;

        edges[from].push_back({to, cost});        
    }

    std::vector<long long> shortestDistance(numberOfVertices + 1, 2000000000000);
    std::vector<long long> leftTravels(numberOfVertices + 1, 2000000000000);
    shortestDistance[startVertice] = 0;
    leftTravels[startVertice] = numberOfTravels;
    std::set<std::pair<long long,long long>> queueOfVertices;
    queueOfVertices.insert({shortestDistance[startVertice], startVertice});

    while (!queueOfVertices.empty()) {
        long long vertice = queueOfVertices.begin()->second;
        queueOfVertices.erase(queueOfVertices.begin());

        for (size_t j = 0; j < edges[vertice].size(); ++j) {
            long long to = edges[vertice][j].first;
            long long cost = edges[vertice][j].second;

            if (shortestDistance[vertice] + cost < shortestDistance[to] && leftTravels[vertice] > 0) {
                leftTravels[to] = leftTravels[vertice] - 1;
                queueOfVertices.erase({shortestDistance[to], to});
                shortestDistance[to] = shortestDistance[vertice] + cost;
                queueOfVertices.insert({shortestDistance[to], to});
            }
        }
    }

    if (shortestDistance[endVertice] == 2000000000000) {
        std::cout << -1 << std::endl;
    } else {
        std::cout << shortestDistance[endVertice] << std::endl;
    }

    return 0;
}