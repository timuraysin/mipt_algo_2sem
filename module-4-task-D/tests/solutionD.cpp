#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/solutionD.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    int numVertices = 3;
    int queriesNumber = 2;

    std::vector<std::vector<int>> graph = {
        {1},
        {2},
        {}
    };

    long long a1 = 2;
    long long a2 = 1;
    long long x = 1;
    long long y = 1;
    long long z = 0;

    REQUIRE(solve(numVertices, queriesNumber, graph, a1, a2, x, y, z) == 2);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    int numVertices = 3;
    int queriesNumber = 1;

    std::vector<std::vector<int>> graph = {
        {1},
        {2},
        {}
    };

    long long a1 = 0;
    long long a2 = 1;
    long long x = 1;
    long long y = 1;
    long long z = 0;

    REQUIRE(solve(numVertices, queriesNumber, graph, a1, a2, x, y, z) == 0);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    int numVertices = 3;
    int queriesNumber = 1;

    std::vector<std::vector<int>> graph = {
        {1},
        {2},
        {}
    };

    long long a1 = 2;
    long long a2 = 1;
    long long x = 1;
    long long y = 1;
    long long z = 0;

    REQUIRE(solve(numVertices, queriesNumber, graph, a1, a2, x, y, z) == 1);
}