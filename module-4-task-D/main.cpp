#include <iostream>
#include <vector>

void dfs(int v, int p, std::vector<int>& tin, std::vector<int>& tout, int& timer, int& log,
    std::vector<std::vector<int>>& table, std::vector<std::vector<int>>& graph) {        
    tin[v] = ++timer;
    table[v][0] = p;
    for (int i = 1; i <= log; ++i) {
        table[v][i] = table[table[v][i - 1]][i - 1];
    }

    for (size_t i = 0; i < graph[v].size(); ++i) {
        int to = graph[v][i];
        if (to != p) {
            dfs(to, v, tin, tout, timer, log, table, graph);
        }
    }
    tout[v] = ++timer;
}

bool upper (int a, int b, std::vector<int>& tin, std::vector<int>& tout) {
    return tin[a] <= tin[b] && tout[a] >= tout[b];
}

int lca(int a, int b, std::vector<int>& tin, std::vector<int>& tout, std::vector<std::vector<int>>& table, int log) {
    if (upper(a, b, tin, tout)) {
        return a;
    }
    if (upper (b, a, tin, tout)) {
        return b;
    } 
    for (int i= log; i >= 0; --i) {
        if (!upper (table[a][i], b, tin, tout)) {
            a = table[a][i];
        }
    }
    return table[a][0];
}

int solve(int numVertices, int queriesNumber, std::vector<std::vector<int>>& graph, 
    long long a1, long long a2, long long x, long long y, long long z) {
    std::vector<int> tin = std::vector<int>(numVertices);
    std::vector<int> tout = std::vector<int>(numVertices);
    std::vector<std::vector<int>> table = std::vector<std::vector<int>>(numVertices);

    int log = 1;
    while ((1 << log) <= numVertices) {
        log++;
    }

    for (int i=0; i < numVertices; ++i)  {
        table[i].resize(log + 1);
    }

    int timer = 0;
    dfs(0, -1, tin, tout, timer, log, table, graph);

    long long t = 0;

    int v = 0;

    for (int i = 1; i <= queriesNumber; i++) {
        v = lca((a1 + v) % numVertices, a2, tin, tout, table, log);
        t += (long long)v;

        long long c = a1;
        a1 = (a1 * x + a2 * y + z) % numVertices;
        a2 = (a2 * x + a1 * y + z) % numVertices;
    }

    return t;
}

int main() {    
    int numVertices;
    int queriesNumber;
    std::cin >> numVertices >> queriesNumber;

    std::vector<std::vector<int>> graph(numVertices);

    for (int i = 1; i <= numVertices - 1; ++i) {
        int vertice;
        std::cin >> vertice;
        graph[vertice].push_back(i);
    }

    long long a1, a2;
    std::cin >> a1 >> a2;

    long long x, y, z;
    std::cin >> x >> y >> z;

    std::cout << solve(numVertices, queriesNumber, graph, a1, a2, x, y, z) << std::endl;

    return 0;
}