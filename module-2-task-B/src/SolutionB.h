#include <set>
#include <vector>

long long SolutionB(long long numberOfVertices, long long numberOfEdges, long long numberOfTravels,
    long long startVertice, long long endVertice, 
    std::vector<std::vector<std::pair<long long, long long>>>& edges) {
    // shortestDistance - массив минимальных расстояний от вершины numberOfVertices
    // leftTravels - массив, содержащий информацию об оставшихся полетах из вершины
    std::vector<long long> shortestDistance(numberOfVertices + 1, 2000000000000);
    std::vector<long long> leftTravels(numberOfVertices + 1, 2000000000000);
    shortestDistance[startVertice] = 0;
    leftTravels[startVertice] = numberOfTravels;
    // очередь вершин, которые надо обработать
    std::set<std::pair<long long,long long>> queueOfVertices;
    queueOfVertices.insert({shortestDistance[startVertice], startVertice});

    while (!queueOfVertices.empty()) {
        long long vertice = queueOfVertices.begin()->second;
        queueOfVertices.erase(queueOfVertices.begin());

        // пытаемся провести релаксацию ребер вершины
        for (size_t j = 0; j < edges[vertice].size(); ++j) {
            long long to = edges[vertice][j].first;
            long long cost = edges[vertice][j].second;

            if (shortestDistance[vertice] + cost < shortestDistance[to] && leftTravels[vertice] > 0) {
                leftTravels[to] = leftTravels[vertice] - 1;
                queueOfVertices.erase({shortestDistance[to], to});
                shortestDistance[to] = shortestDistance[vertice] + cost;
                queueOfVertices.insert({shortestDistance[to], to});
            }
        }
    }

    if (shortestDistance[endVertice] == 2000000000000) {
        return -1;
    } else {
        return shortestDistance[endVertice];
    }
}