#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/SolutionB.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    long long vertices = 5;
    long long edges = 7;
    long long travels = 2;
    long long start = 4;
    long long end = 1;

    std::vector<std::vector<std::pair<long long, long long>>> testestEdges(vertices + 1);
    test[1].push_back({2, 6});
    testEdges[5].push_back({1, 1});
    testEdges[4].push_back({1, 9});
    testEdges[4].push_back({5, 3});
    testEdges[4].push_back({3, 2});
    testEdges[2].push_back({5, 7});
    testEdges[3].push_back({5, 1});


    REQUIRE(SolutionB(vertices, edges, travels, start, end, testEdges) == 4);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    long long vertices = 3;
    long long edges = 3;
    long long travels = 1;
    long long start = 1;
    long long end = 3;

    std::vector<std::vector<std::pair<long long, long long>>> testEdges(vertices + 1);
    testEdges[1].push_back({2, 4});
    testEdges[2].push_back({3, 5});
    testEdges[3].push_back({1, 6});


    REQUIRE(SolutionB(vertices, edges, travels, start, end, testEdges) == -1);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    long long vertices = 5;
    long long edges = 1;
    long long travels = 1;
    long long start = 2;
    long long end = 3;

    std::vector<std::vector<std::pair<long long, long long>>> testEdges(vertices + 1);
    testEdges[1].push_back({2, 4});

    REQUIRE(SolutionB(vertices, edges, travels, start, end, testEdges) == -1);
}