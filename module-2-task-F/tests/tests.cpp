#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/geometry.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    CPoint p(1, 1);

    REQUIRE(p.x == 1);
    REQUIRE(p.y == 1);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    CPolygon p(CPoint(1, 1), CPoint(2, 2), CPoint(3, 1));

    REQUIRE(p.containsPoint(CPoint(0, 0)) == false);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    CTriangle tr(CPoint(1, 1), CPoint(2, 2), CPoint(3, 1));
    CPolygon p(CPoint(1, 1), CPoint(2, 2), CPoint(3, 1));

    REQUIRE(tr == p);
}