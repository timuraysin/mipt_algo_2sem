/* Я сдал после дедлайна, на 0.5 балла */
#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <typeinfo>
#include <vector>

const double Eps = 0.001;

struct CPoint {

public:    
    double x;
    double y;

    CPoint();    
    CPoint(double x, double y);

    double GetDistance(const CPoint& p2) const;

};

CPoint::CPoint() : 
    x(0), 
    y(0)
{
}

CPoint::CPoint(double x, double y) :
    x(x),
    y(y)
{
}

double CPoint::GetDistance(const CPoint& p2) const {
    double val = (x - p2.x) * (x - p2.x) + (y - p2.y) * (y - p2.y);        
    return std::sqrt(val);
}

bool operator==(const CPoint& p1, const CPoint& p2) {    
    return std::abs(p1.x - p2.x) <= Eps && std::abs(p1.y - p2.y) <= Eps;
}

bool operator!=(const CPoint& p1, const CPoint& p2) {
    return !(p1 == p2);
}

CPoint RotatePoint(CPoint point, CPoint Center, double angle) {
    double xN = (point.x - Center.x) * cos(angle) - (point.y - Center.y) * sin(angle) + Center.x;
    double yN = (point.x - Center.x) * sin(angle) + (point.y - Center.y) * cos(angle) + Center.y;

    return CPoint(xN, yN);
}

void Swap(CPoint& p1, CPoint& p2) {
    CPoint t = p1;
    p1 = p2;
    p2 = t;
}

struct CLine {

public:
    double a;
    double b;
    double c;

    CLine(double k, double shift);
    CLine(CPoint one, CPoint two);    
    CLine(CPoint one, double k);

    friend bool operator==(const CLine& l1, const CLine& l2);

};

CLine::CLine(double k, double shift) :
    a(k),
    b(-1),
    c(shift)
{
}

CLine::CLine(CPoint one, CPoint two) {
    a = one.y - two.y;
    b = two.x - one.x;
    c = one.x * two.y - one.y * two.x;
}

CLine::CLine(CPoint one, double k) {
    double shift = one.y - k * one.x;
    a = k;
    b = -1;
    c = shift;
}

bool operator==(const CLine& l1, const CLine& l2) {
    if (std::abs(l1.a * l2.b - l1.b * l2.a) >= Eps) {
        return false;
    }

    if (std::abs(l1.a * 0 + l1.b * (-l1.c / l1.b) + l1.c) <= Eps && 
        std::abs(l2.a * 0 + l2.b * (-l1.c / l1.b) + l2.c) <= Eps) {
        return true;
    }
    return false;
}

bool operator!=(const CLine& l1, const CLine& l2) {
    return !(l1 == l2);
}

double Det(double a, double b, double c, double a1, double b1, double c1, double a2, double b2, double c2) {
    return a * (b1 * c2 - b2 * c1) - b * (a1 * c2 - a2 * c1) + c * (a1 * b2 - a2 * b1);
}

CLine GetPerpendicularLine(CPoint p, CLine l) {
    double vx = l.a;
    double vy = l.b;

    return CLine(p, CPoint( p.x + vx, p.y + vy));
}

class CShape {

public:
    virtual ~CShape() {}

    virtual double Perimeter() = 0;
    virtual double Area() = 0;
    virtual bool operator==(const CShape& another) const = 0;
    virtual bool operator!=(const CShape& another) const = 0;
    virtual bool IsCongruentTo(const CShape& another) = 0;
    virtual bool IsSimilarTo(const CShape& another) = 0;
    virtual bool ContainsPoint(CPoint point) const = 0;
    virtual void Rotate(CPoint Center, double angle) = 0;
    virtual void Reflex(CPoint Center) = 0;
    virtual void Reflex(CLine axis) = 0;
    virtual void Scale(CPoint Center, double k) = 0;

};

enum TEdgeType {
    TOUCHING, CROSSING, INDIFFERENT
};

enum TRelativePosition {
    LEFT, RIGHT, BETWEEN, ORIGIN, DESTINATION, BEHIND, BEYOND
};

class CPolygon : public CShape {

public:
    CPolygon(std::vector<CPoint>& points);
    template<typename ... Points>
    CPolygon(Points const & ... args);

    int VerticesCount() const;
    std::vector<CPoint> GetVertices() const;
    bool IsConvex();
    double Perimeter() override;
    double Area() override;
    bool operator==(const CShape& another) const override;
    bool operator!=(const CShape& another) const override;
    bool IsCongruentTo(const CShape& another) override;
    bool ContainsPoint(CPoint point) const override;
    bool IsSimilarTo(const CShape& another) override;
    void Rotate(CPoint Center, double angle) override;
    void Reflex(CPoint Center) override;
    void Reflex(CLine axis) override;
    void Scale(CPoint Center, double k) override;

protected:
    std::vector<CPoint> vertices;

    bool areAnglesEqual(CPoint& v1, CPoint& head, CPoint& v2, CPoint& e1, CPoint& head2, CPoint& e2,
        const CPolygon& other);

    bool areAnglesProportional(CPoint& v1, CPoint& head, CPoint& v2, CPoint& e1, CPoint& head2, CPoint& e2,
        const CPolygon& other);    
    bool checkVertices();
    bool runEquality(int n, const std::vector<CPoint>& vertices) const;
    bool runCongruenty(int n, const std::vector<CPoint>& verticesCopy, const CPolygon& other);
    bool runSimilarity(int n, const std::vector<CPoint>& verticesCopy, const CPolygon& other);

};

bool CPolygon::areAnglesEqual(CPoint& v1, CPoint& head, CPoint& v2, CPoint& e1, CPoint& head2, CPoint& e2,
        const CPolygon& other) {            
    double diffInEdgeLength1 = std::abs(v1.GetDistance(head) - e1.GetDistance(head2));
    double diffInEdgeLength2 = std::abs(head.GetDistance(v2) - head2.GetDistance(e2));

    if (diffInEdgeLength1 >= Eps || diffInEdgeLength2 >= Eps) {
        return false;
    }

    CPoint vector1(v1.x - head.x, v1.y - head.y);
    CPoint vector2(v2.x - head2.x, v2.y - head2.y);

    double scalarMul1 = (vector1.x * vector2.x + vector1.y * vector2.y);
    double moduleMul1 = std::sqrt((vector1.x * vector1.x + vector1.y * vector1.y) * 
        (vector2.x * vector2.x +vector2.y * vector2.y));

    CPoint middlePoint( (v1.x + v2.x) / 2.0, (v1.y + v2.y) / 2.0);

    bool moreThanPI = false;

    if (!ContainsPoint(middlePoint)) {
        moreThanPI = true;
    }

    vector1 = CPoint(v1.x - head.x, v1.y - head.y);
    vector2 = CPoint(v2.x - head2.x, v2.y - head2.y);

    double scalarMul2 = (vector1.x * vector2.x + vector1.y * vector2.y);
    double moduleMul2 = std::sqrt((vector1.x * vector1.x + vector1.y * vector1.y) * 
        (vector2.x * vector2.x +vector2.y * vector2.y));

    CPoint middlePoint2( (e1.x + e2.x) / 2.0, (e1.y + e2.y) / 2.0);

    bool moreThanPI2 = false;

    if (!other.ContainsPoint(middlePoint2)) {
        moreThanPI2 = true;
    }

    if (std::abs(scalarMul2 * moduleMul1 - scalarMul1 * moduleMul2) <= Eps && moreThanPI == moreThanPI2) {
        return true;
    }
    return false;
}

bool CPolygon::areAnglesProportional(CPoint& v1, CPoint& head, CPoint& v2, CPoint& e1, CPoint& head2, CPoint& e2,
        const CPolygon& other) {
    double diff = std::abs(v1.GetDistance(head) / e1.GetDistance(head2) - 
        v2.GetDistance(head) / e2.GetDistance(head2));

    if (diff <= Eps) {
        return false;
    }

    CPoint vector1(v1.x - head.x, v1.y - head.y);
    CPoint vector2(v2.x - head2.x, v2.y - head2.y);

    double scalarMul1 = (vector1.x * vector2.x + vector1.y * vector2.y);
    double moduleMul1 = std::sqrt((vector1.x * vector1.x + vector1.y * vector1.y) * 
        (vector2.x * vector2.x +vector2.y * vector2.y));

    CPoint middlePoint( (v1.x + v2.x) / 2.0, (v1.y + v2.y) / 2.0);

    bool moreThanPI = false;

    if (!ContainsPoint(middlePoint)) {
        moreThanPI = true;
    }

    vector1 = CPoint(v1.x - head.x, v1.y - head.y);
    vector2 = CPoint(v2.x - head2.x, v2.y - head2.y);

    double scalarMul2 = (vector1.x * vector2.x + vector1.y * vector2.y);
    double moduleMul2 = std::sqrt((vector1.x * vector1.x + vector1.y * vector1.y) * 
        (vector2.x * vector2.x +vector2.y * vector2.y));

    CPoint middlePoint2( (e1.x + e2.x) / 2.0, (e1.y + e2.y) / 2.0);

    bool moreThanPI2 = false;

    if (!other.ContainsPoint(middlePoint2)) {
        moreThanPI2 = true;
    }

    if (std::abs(scalarMul2 * moduleMul1 - scalarMul1 * moduleMul2) <= Eps && moreThanPI == moreThanPI2) {
        return true;
    }
    return false;
}

bool CPolygon::checkVertices() {
    if (vertices.size() < 3) {
        return false;
    }

    for (size_t i = 0; i < vertices.size()-1; ++i) {
        CPoint p1 = vertices[i];
        CPoint p2 = vertices[(i+1) % vertices.size()];
        CPoint p3 = vertices[(i+2) % vertices.size()];

        if (CLine(p1, p2) == CLine(p2, p3)) {
            return false;
        }
    }
    return true;
}

CPolygon::CPolygon(std::vector<CPoint>& points) {
    vertices = points;

    try {
        if (!checkVertices()) {
            throw -1;
        }
    } catch (int val) {
        std::cerr << "Wrong pack of vertices" << std::endl;
    }
}

template<typename ... Points>
CPolygon::CPolygon(Points const & ... args) {
    std::vector<CPoint> p { { args... }};
    vertices = p;

    try {
        if (!checkVertices()) {
            throw -1;
        }
    } catch (int val) {
        std::cerr << "Wrong pack of vertices" << std::endl;
    }
}


int CPolygon::VerticesCount() const {
    return vertices.size();
}

std::vector<CPoint> CPolygon::GetVertices() const {
    return vertices;
}

bool CPolygon::IsConvex() {
    int next = 0;
    int nextAfter = 0;
    int flag = 0;
    double angleConvex = 0;

    int n = VerticesCount();

    if (n <= 3) {
        return true;
    }

    for (int i = 0; i < n; ++i) {
        next = (i + 1) % n;
        nextAfter = (i + 2) % n;

        angleConvex = (vertices[next].x - vertices[i].x) * (vertices[nextAfter].y - vertices[nextAfter].y);
        angleConvex -= (vertices[next].y - vertices[i].y) * (vertices[nextAfter].x - vertices[nextAfter].x);

        if (angleConvex < 0) {
            flag |= 1;
        } else if (angleConvex > 0) {
            flag |= 2;
        }

        if (flag == 3) {
            return false;
        }
    }

    return true;
}

double CPolygon::Perimeter() {
    double res = 0;

    for (int i = 0; i < VerticesCount(); ++i) {
        res += vertices[i].GetDistance((vertices[(i+1) % VerticesCount()]));
    }

    return res;
}

double CPolygon::Area() {
    double Area = 0;

    // Формула Гаусса для площади
    for (int i = 0; i < VerticesCount(); ++i) {
        Area += (vertices[i].x + vertices[(i + 1) % VerticesCount()].x) * 
            (vertices[i].y - vertices[(i + 1) % VerticesCount()].y);
    }

    Area = std::abs(Area) / 2;    

    return Area;
}

bool CPolygon::runEquality(int n, const std::vector<CPoint>& otherVertices) const {
    for (int j = 0; j < n; j++) {
        int ind = j;
        bool ans = true;

        for (int i = 0; i < n; i++) {
            if (vertices[i] != otherVertices[ind]) {
                ans = false;
                break;
            }
            ind = (ind + 1) % n;
        }

        if (ans) {
            return true;
        }
    }
    return false;
}

bool CPolygon::operator==(const CShape& another) const {
    try {
        const CPolygon& other = dynamic_cast<const CPolygon&>(another);

        if (VerticesCount() != other.VerticesCount()) {
            return false;
        }

        int n = VerticesCount();

        if (runEquality(n, other.vertices)) {
            return true;
        }

        std::vector<CPoint> reversedPoints = GetVertices();
        std::reverse(reversedPoints.begin(), reversedPoints.end());

        if (runEquality(n, reversedPoints)) {
            return true;
        }
        return false;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

bool CPolygon::operator!=(const CShape& another) const {
    return !(*this == another);
}

bool CPolygon::runCongruenty(int n, const std::vector<CPoint>& verticesCopy, const CPolygon& other) {
    for (int j = 0; j < n; j++) {
        bool ans = true;
        int ind = j;

        for (int i = 0; i < n; i++) {
            CPoint p1 = verticesCopy[i];
            CPoint h = verticesCopy[(i + 1) % n];
            CPoint p2 = verticesCopy[(i + 2) % n];

            CPoint e1 = other.vertices[(ind + i) % n];
            CPoint h2 = other.vertices[(ind + i + 1) % n];
            CPoint e2 = other.vertices[(ind + i + 2) % n];

            if (!areAnglesEqual(p1, h, p2, e1, h2, e2, other)) {
                ans = false;
                break;
            }
        }

        if (ans) {
            return true;
        }
    }
    return false;
}

bool CPolygon::IsCongruentTo(const CShape& another) {
    try {
        const CPolygon& other = dynamic_cast<const CPolygon&>(another);

        if (VerticesCount() != other.VerticesCount()) {
            return false;
        }

        int n = VerticesCount();

        if (runCongruenty(n, vertices, other)) {
            return true;
        }

        std::vector<CPoint> verticesRotated = GetVertices();
        std::reverse(verticesRotated.begin(), verticesRotated.end());        

        if (runCongruenty(n, verticesRotated, other)) {
            return true;
        }

        Reflex(CLine(CPoint(0, 0), CPoint(0, 1)));

        if (runCongruenty(n, vertices, other)) {
            return true;
        }

        verticesRotated = GetVertices();
        std::reverse(verticesRotated.begin(), verticesRotated.end());

        if (runCongruenty(n, verticesRotated, other)) {
            return true;
        }

        Reflex(CLine(CPoint(0, 0), CPoint(0, 1)));
        return false;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

bool CPolygon::ContainsPoint(CPoint point) const {
    int res = 0;

    for (int i = 0; i < VerticesCount(); ++i) {
        CPoint p1 = vertices[i];
        CPoint p2 = vertices[(i + 1) % VerticesCount()];

        if (p1.y > p2.y) {
            Swap(p1, p2);
        }

        if (p2.y <= point.y || point.y < p1.y) {
            continue;
        }

        double orientation = (point.x - p1.x) * (p2.y - p1.y) - (point.y - p1.y) * (p2.x - p1.x);
        if (orientation >= Eps) {
            res++;
        } else if (orientation <= Eps && orientation >= -0.0) {
            return true;
        }
    }

    return (res % 2);
}

bool CPolygon::runSimilarity(int n, const std::vector<CPoint>& verticesCopy, const CPolygon& other) {
    for (int j = 0; j < n; j++) {
        bool ans = true;
        int ind = j;

        for (int i = 0; i < n; i++) {
            CPoint p1 = verticesCopy[i];
            CPoint h = verticesCopy[(i + 1) % n];
            CPoint p2 = verticesCopy[(i + 2) % n];

            CPoint e1 = other.vertices[(ind + i) % n];
            CPoint h2 = other.vertices[(ind + i + 1) % n];
            CPoint e2 = other.vertices[(ind + i + 2) % n];

            if (!areAnglesProportional(p1, h, p2, e1, h2, e2, other)) {
                ans = false;
                break;
            }
        }

        if (ans) {
            return true;
        }
    }
    return false;
}

bool CPolygon::IsSimilarTo(const CShape& another) {
    try {
        const CPolygon& other = dynamic_cast<const CPolygon&>(another);

        if (VerticesCount() != other.VerticesCount()) {
            return false;
        }

        int n = VerticesCount();

        if (runSimilarity(n, vertices, other)) {
            return true;
        }

        std::vector<CPoint> verticesRotated = GetVertices();
        std::reverse(verticesRotated.begin(), verticesRotated.end());

        if (runSimilarity(n, verticesRotated, other)) {
            return true;
        }

        Reflex(CLine(CPoint(0, 0), CPoint(0, 1)));

        if (runSimilarity(n, vertices, other)) {
            return true;
        }
        
        verticesRotated = GetVertices();
        std::reverse(verticesRotated.begin(), verticesRotated.end());

        if (runSimilarity(n, verticesRotated, other)) {
            return true;
        }

        Reflex(CLine(CPoint(0, 0), CPoint(0, 1)));

        return false;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

void CPolygon::Rotate(CPoint Center, double angle) {
    angle = (angle * M_PI / 180.0);

    for (int i = 0; i < VerticesCount(); ++i) {
        vertices[i] = RotatePoint(vertices[i], Center, angle);
    }
}

void CPolygon::Reflex(CPoint Center) {
    for (int i = 0; i < VerticesCount(); ++i) {
        vertices[i].x = 2 * Center.x - vertices[i].x;
        vertices[i].y = 2 * Center.y - vertices[i].y;
    }
}

void CPolygon::Reflex(CLine axis) {
    for (int i = 0; i < VerticesCount(); ++i) {
        CLine l = GetPerpendicularLine(vertices[i], axis);

        double nX = (l.b * axis.c - l.c * axis.b) / (l.a * axis.b - l.b * axis.a);
        double nY = (l.c * axis.a - l.a * axis.c) / (l.a * axis.b - l.b * axis.a);

        vertices[i] = CPoint(2 * nX - vertices[i].x, 2 * nY - vertices[i].y);
    }
}

void CPolygon::Scale(CPoint Center, double k) {
    for (int i = 0; i < VerticesCount(); ++i) {
        double x = vertices[i].x;
        double y = vertices[i].y;

        double vx = x - Center.x;
        double vy = y - Center.y;

        vx *= k;
        vy *= k;

        vertices[i] = CPoint(vx + Center.x, vy + Center.y);
    }
}

class CEllipse : public CShape {

public:
    CEllipse(CPoint p1, CPoint p2, double f);    

    double Perimeter() override;
    double Area() override;
    bool operator==(const CShape& another) const override;
    bool operator!=(const CShape& another) const override;
    bool IsCongruentTo(const CShape& another) override;
    bool IsSimilarTo(const CShape& another) override;
    bool ContainsPoint(CPoint point) const override;
    void Rotate(CPoint Center, double angle) override;
    void Reflex(CPoint Center) override;
    void Reflex(CLine axis) override;
    void Scale(CPoint Center, double k) override;
    std::pair<CPoint, CPoint> Focuses();
    std::pair<CLine, CLine> Directrices();
    double Eccentricity();
    CPoint Center() const;

protected:
    CPoint p1;
    CPoint p2;
    double a;
    double b;
    double c;
    double e;
    
};

CEllipse::CEllipse(CPoint p1, CPoint p2, double f) {
    a = f / 2;
    this->p1 = p1;
    this->p2 = p2;

    c = p1.GetDistance(CPoint( (p1.x + p2.x) / 2, (p1.y + p2.y) / 2));
    b = sqrt(a * a - c * c);
    e = c / a;
}

    
double CEllipse::Perimeter() {
    double val = M_PI * ( 3 * (a + b) - sqrt( (3 * a + b) * (a + 3 * b)) );
    return val;
}

double CEllipse::Area() {
    return M_PI * a * b;
}

bool CEllipse::operator==(const CShape& another) const {
    try {
        const CEllipse& el = dynamic_cast<const CEllipse&>(another);
        if (((el.p1 == p1 && el.p2 == p2) || (el.p1 == p2 && el.p2 == p1)) && (a == el.a)) {
            return true;
        }
        return false;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

bool CEllipse::operator!=(const CShape& another) const {
    return !(*this == another);
}

bool CEllipse::IsCongruentTo(const CShape& another) {
    try {
        const CEllipse& el = dynamic_cast<const CEllipse&>(another);

        if (a == el.a && p1.GetDistance(p2) == el.p1.GetDistance(el.p2)) {
            return true;
        }
        return false;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

bool CEllipse::IsSimilarTo(const CShape& another) {
    try {
        const CEllipse& el = dynamic_cast<const CEllipse&>(another);

        if ( std::abs(a / el.a - b / el.b) <= Eps ) {
            return true;
        }
        return false;
    } catch(const std::bad_cast& e) {
        return false;
    }
}

bool CEllipse::ContainsPoint(CPoint point) const {
    if (p1 == p2) {
        return (point.x - p1.x) * (point.x - p1.x) + (point.y - p1.y) * (point.y - p1.y) <= a * a;
    }

    return (point.GetDistance(p1) + point.GetDistance(p2) <= 2 * a);
}

void CEllipse::Rotate(CPoint Center, double angle) {
    angle = (angle * M_PI / 180.0);
    RotatePoint(p1, Center, angle);
    RotatePoint(p2, Center, angle);

    c = p1.GetDistance(CPoint( (p1.x + p2.x) / 2, (p1.y + p2.y) / 2) );
    b = sqrt(a * a - c * c);
    e = c / a;
}

void CEllipse::Reflex(CPoint Center) {
    p1.x = 2 * Center.x - p1.x;
    p1.y = 2 * Center.y - p1.y;

    p2.x = 2 * Center.x - p2.x;
    p2.y = 2 * Center.y - p2.y;

    c = p1.GetDistance(CPoint( (p1.x + p2.x) / 2, (p1.y + p2.y) / 2) );
    b = sqrt(a * a - c * c);
    e = c / a;
}

void CEllipse::Reflex(CLine axis) {
    CLine l = GetPerpendicularLine(p1, axis);

    double nX = (l.b * axis.c - l.c * axis.b) / (l.a * axis.b - l.b * axis.a);
    double nY = (l.c * axis.a - l.a * axis.c) / (l.a * axis.b - l.b * axis.a);

    p1 = CPoint(2 * nX - p1.x, 2 * nY - p1.y);

    l = GetPerpendicularLine(p2, axis);

    nX = (l.b * axis.c - l.c * axis.b) / (l.a * axis.b - l.b * axis.a);
    nY = (l.c * axis.a - l.a * axis.c) / (l.a * axis.b - l.b * axis.a);

    p2 = CPoint(2 * nX - p2.x, 2 * nY - p2.y);

    c = p1.GetDistance(CPoint( (p1.x + p2.x) / 2, (p1.y + p2.y) / 2) );
    b = sqrt(a * a - c * c);
    e = c / a;
}

void CEllipse::Scale(CPoint Center, double k) {
    double x = p1.x;
    double y = p1.y;

    double vx = x - Center.x;
    double vy = y - Center.y;

    vx *= k;
    vy *= k;

    p1 = CPoint(vx + Center.x, vy + Center.y);

    x = p2.x;
    y = p2.y;

    vx = x - Center.x;
    vy = y - Center.y;

    vx *= k;
    vy *= k;

    p2 = CPoint(vx + Center.x, vy + Center.y);

    a *= k;
    c = p1.GetDistance(CPoint( (p1.x + p2.x) / 2, (p1.y + p2.y) / 2) );
    b = sqrt(a * a - c * c);
    e = c / a;
}

std::pair<CPoint, CPoint> CEllipse::Focuses() {
    std::pair<CPoint, CPoint> p = {p1, p2};
    return p;
}

std::pair<CLine, CLine> CEllipse::Directrices() {
    double dist = a / e;
    if (p1.x > p2.x) {
        Swap(p1, p2);
    }

    CPoint c = Center();

    double vx = (p1.x - c.x);
    double vy = (p1.y - c.y);

    double l = sqrt(vx * vx + vy * vy);
    vx *= dist / l;
    vy *= dist / l;

    p1.x = c.x + vx;
    p1.y = c.y + vy;

    CLine l1 = GetPerpendicularLine(p1, CLine(p1, p2));

    vx = (p2.x - c.x);
    vy = (p2.y - c.y);
    l = sqrt(vx * vx + vy * vy);
    vx *= dist / l;
    vy *= dist / l;

    p2.x = c.x + vx;
    p2.y = c.y + vy;

    CLine l2 = GetPerpendicularLine(p2, CLine(p1, p2));

    std::pair<CLine, CLine> lines = {l1, l2};

    return lines;
}

double CEllipse::Eccentricity() {
    return e;
}

CPoint CEllipse::Center() const {
    return CPoint((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
}

class CCircle : public CEllipse {
public:
    CCircle(CPoint p, double r);
    double Radius();
};

CCircle::CCircle(CPoint p, double r) : 
    CEllipse(p, p, 2 * r) 
{
}

double CCircle::Radius() {
    return a;
}

class CRectangle : public CPolygon {
public:
    CRectangle(CPoint a, CPoint b, double k);

    CPoint Center();
    std::pair<CLine, CLine> Diagonals();

};

CRectangle::CRectangle(CPoint a, CPoint b, double k) :
    CPolygon( a, CPoint(a.x, b.y), b, CPoint(b.x, a.y)) {
    if (k < 1) {
        k = 1 / k;
    }

    double mi = sqrt( ((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)) / (k * k + 1) );
    double ma = mi * k;
    double angle = atan(k);

    CPoint c = a;
    CPoint d = b;

    c = RotatePoint(b, a, angle);
    d = RotatePoint(a, b, angle);

    double vx = c.x - a.x;
    double vy = c.y - a.y;

    double le = sqrt(vx * vx + vy * vy);

    vx *= mi / le;
    vy *= mi / le;

    c.x = vx + a.x;
    c.y = vy + a.y;

    vx = d.x - b.x;
    vy = d.y - b.y;

    le = sqrt(vx * vx + vy * vy);

    vx *= ma / le;
    vy *= mi / le;

    d.x = vx + b.x;
    d.y = vy + b.y;

    vertices[1] = c;
    vertices[3] = d;
}


CPoint CRectangle::Center() {
    CPoint a = vertices[0];
    CPoint b = vertices[2];
    return CPoint( (a.x + b.x) / 2, (a.y + b.y) / 2);
}

std::pair<CLine, CLine> CRectangle::Diagonals() {
    CLine l1(vertices[0], vertices[2]);
    CLine l2(vertices[1], vertices[3]);

    std::pair<CLine, CLine> l = {l1, l2};

    return l;
}

class CSquare : public CRectangle {
public:
    CSquare(CPoint a, CPoint b);

    CCircle CircumscribedCircle();
    CCircle InscribedCircle();

};

CSquare::CSquare(CPoint a, CPoint b) :
    CRectangle(a, b, 1.0) 
{
}

CCircle CSquare::CircumscribedCircle() {
    CPoint c = Center();
    return CCircle(c, c.GetDistance(vertices[0]));
}

CCircle CSquare::InscribedCircle() {
    CPoint c = Center();
    return CCircle(c, vertices[0].GetDistance(vertices[1]) / 2);
}

class CTriangle : public CPolygon {
public:
    CTriangle(std::vector<CPoint> points);
    template<typename ... Points>
    CTriangle(Points const & ... args);

    CCircle CircumscribedCircle();
    CCircle InscribedCircle();
    CPoint Centroid();
    CPoint Orthocenter();
    CLine EulerLine();
    CCircle NinePointsCircle();

};

CTriangle::CTriangle(std::vector<CPoint> points) : 
    CPolygon(points) 
{
}

template<typename ... Points>
CTriangle::CTriangle(Points const & ... args) {
    std::vector<CPoint> p { { args... }};
    vertices = p;

    try {
        if (!checkVertices()) {
            throw -1;
        }
    } catch (int val) {
        std::cerr << "Wrong pack of vertices" << std::endl;
    }
}

CCircle CTriangle::CircumscribedCircle() {
    double x0 = Det( vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y, vertices[0].y, 1,
        vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y, vertices[1].y, 1,
        vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y, vertices[2].y, 1) / 
        (2 * Det(vertices[0].x, vertices[0].y, 1, vertices[1].x, vertices[1].y, 1, vertices[2].x, vertices[2].y, 1));
    double y0 = Det( vertices[0].x, vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y, 1,
        vertices[1].x, vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y, 1,
        vertices[2].x,vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y, 1) / 
        (2 * Det(vertices[0].x, vertices[0].y, 1, vertices[1].x, vertices[1].y, 1, vertices[2].x, vertices[2].y, 1));

    CPoint p(x0, y0);

    return CCircle(p, p.GetDistance(vertices[0]));
}

CCircle CTriangle::InscribedCircle() {
    double a = vertices[0].GetDistance(vertices[1]);
    double b = vertices[1].GetDistance(vertices[2]);
    double c = vertices[2].GetDistance(vertices[0]);

    double r = sqrt( (-a + b + c) * (a - b + c) * (a + b - c) / (4 * (a + b + c) ));
    a = vertices[1].GetDistance(vertices[2]);
    b = vertices[0].GetDistance(vertices[2]);
    c = vertices[0].GetDistance(vertices[1]);

    double x0 = (vertices[0].x * a + vertices[1].x * b + vertices[2].x * c) / (a + b + c);
    double y0 = (vertices[0].y * a + vertices[1].y * b + vertices[2].y * c) / (a + b + c);

    return CCircle(CPoint(x0, y0), r);
}

CPoint CTriangle::Centroid() {
    CPoint a = vertices[0];
    CPoint b = vertices[1];
    CPoint c = vertices[2];
    return CPoint( (a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3);
}

CPoint CTriangle::Orthocenter() {
    double x0 = Det(vertices[0].y, vertices[1].x * vertices[2].x + vertices[0].y * vertices[0].y,1,
        vertices[1].y, vertices[0].x * vertices[2].x + vertices[1].y * vertices[1].y,  1,
        vertices[2].y, vertices[0].x * vertices[1].x + vertices[2].y * vertices[2].y, 1) /
        Det(vertices[0].x, vertices[0].y, 1, vertices[1].x, vertices[1].y, 1, vertices[2].x, vertices[2].y, 1);
    double y0 = Det(vertices[0].x * vertices[0].x + vertices[1].y * vertices[2].y, vertices[0].x, 1,
        vertices[1].x * vertices[1].x + vertices[0].y * vertices[2].y, vertices[1].x, 1,
        vertices[2].x * vertices[2].x + vertices[0].y * vertices[1].y, vertices[2].x, 1) /
        Det(vertices[0].x, vertices[0].y, 1, vertices[1].x, vertices[1].y, 1, vertices[2].x, vertices[2].y, 1);
    return CPoint(x0, y0);
}

CLine CTriangle::EulerLine() {    
    double x0 = Det(vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y, vertices[0].y, 1,
        vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y, vertices[1].y, 1,
        vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y, vertices[2].y, 1) / 
        (2 * Det(vertices[0].x, vertices[0].y, 1, vertices[1].x, vertices[1].y, 1, vertices[2].x, vertices[2].y, 1));
    double y0 = Det(vertices[0].x, vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y, 1,
        vertices[1].x, vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y, 1,
        vertices[2].x,vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y, 1) /
        (2 * Det(vertices[0].x, vertices[0].y, 1, vertices[1].x, vertices[1].y, 1, vertices[2].x, vertices[2].y, 1));

    CPoint p(x0, y0);

    return CLine(p, Orthocenter());
}

CCircle CTriangle::NinePointsCircle() {
    double x0 = Det(vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y, vertices[0].y, 1,
        vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y, vertices[1].y, 1,
        vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y, vertices[2].y, 1) / 
        (2 * Det(vertices[0].x, vertices[0].y, 1, vertices[1].x, vertices[1].y, 1, vertices[2].x, vertices[2].y, 1));
    double y0 = Det(vertices[0].x, vertices[0].x * vertices[0].x + vertices[0].y * vertices[0].y, 1,
        vertices[1].x, vertices[1].x * vertices[1].x + vertices[1].y * vertices[1].y, 1,
        vertices[2].x,vertices[2].x * vertices[2].x + vertices[2].y * vertices[2].y, 1) / 
        (2 * Det(vertices[0].x, vertices[0].y, 1, vertices[1].x, vertices[1].y, 1, vertices[2].x, vertices[2].y, 1));

    CPoint p(x0, y0);
    CPoint o = Orthocenter();
    CPoint cent( (p.x + o.x) / 2, (p.y + o.y) / 2);

    return CCircle(cent, cent.GetDistance(vertices[0]));
}