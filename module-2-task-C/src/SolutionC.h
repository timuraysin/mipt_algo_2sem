#include <set>
#include <vector>

double SolutionC(long long numberOfVertices, long long startVertice, long long endVertice,
    std::vector<std::vector<double>>& shortestDistance) {
    // Алгоритм Флойда-Уоршелла
    for (int k = 1; k <= numberOfVertices; ++k) {
        for (int i = 1; i <= numberOfVertices; ++i) {
            for (int j = 1; j <= numberOfVertices; ++j) {
                if (shortestDistance[i][k] < 2000000000000.0 && shortestDistance[k][j] < 2000000000000.0) {
                    // считаем по формуле из теории вероятностей вероятность быть избитым
                    shortestDistance[i][j] = std::min(shortestDistance[i][j],
                        shortestDistance[i][k] + shortestDistance[k][j] -
                        shortestDistance[i][k] * shortestDistance[k][j]);
                }
            }
        }
    }

    return shortestDistance[startVertice][endVertice];
}