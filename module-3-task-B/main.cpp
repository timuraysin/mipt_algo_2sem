#include <algorithm>
#include <iostream>
#include <vector>

struct Edge {

public:
    Edge(int f, int t, long long w);

    int from;
    int to;
    long long weight;
};

Edge::Edge(int f, int t, long long w) :
    from(f),
    to(t),
    weight(w)
{
}

bool operator<(const Edge& e1, const Edge& e2) {
    return (e1.weight < e2.weight);
}

class DSU {

public:
    explicit DSU(const size_t &size);

    size_t find(const size_t &v);
    void unite(const size_t &v, const size_t &u);

private:
    std::vector<size_t> p;
    std::vector<size_t> r;
};

DSU::DSU(const size_t &size) {
    p.resize(size);
    r.resize(size, 1);

    for (size_t i = 0; i < size; ++i) {
        p[i] = i;
    }        
}

size_t DSU::find(const size_t &v) {
    if (p[v] == v) {/// True only if v is a root.
        return v;
    }

    return p[v] = find(p[v]);
}

void DSU::unite(const size_t &v, const size_t &u) {
    const size_t firstId = find(v);
        const size_t secondId = find(u);

        if (firstId == secondId)
            return;

        const size_t firstRank = r[firstId];
        const size_t secondRank = r[secondId];

        if (firstRank > secondRank) {
            p[secondId] = firstId;
            r[firstId] += secondRank;
        }

        else {
            p[firstId] = secondId;
            r[secondId] += firstRank;
        }
}

long long Cruscal(int numVertices, int numEdges, std::vector<Edge>& edges) {
    std::sort(edges.begin(), edges.end());
    DSU dsu(numVertices + 1);
    long long res = 0;

    for (int i = 0; i < numEdges * 2; ++i) {
        int u = edges[i].from;
        int v = edges[i].to;
        long long weight = edges[i].weight;

        if (dsu.find(u) != dsu.find(v)) {
            res += weight;
            dsu.unite(u, v);
        }
    }

    return res;
}

int main() {
    int numVertices = 0;
    int numEdges = 0;

    std::cin >> numVertices >> numEdges;

    std::vector<Edge> edges;

    for (int i = 0; i < numEdges; ++i) {
        int from;
        int to;
        long long weight;

        std::cin >> from >> to >> weight;

        edges.push_back(Edge(from, to, weight));
        edges.push_back(Edge(to, from, weight));
    }

    std::cout << Cruscal(numVertices, numEdges, edges) << std::endl;

    return 0;
}