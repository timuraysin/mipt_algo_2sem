#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "../src/Cruscal.h"

TEST_CASE("Test 1") {
    INFO("TEST_CASE: Test 1 started!");
    
    std::vector<Edge> edges = { 
        Edge(1, 2, 1), 
        Edge(2, 1, 1),
        Edge(2, 3, 1),
        Edge(3, 2, 1), 
        Edge(1, 3, 3),
        Edge(3, 1, 3)
    };

    REQUIRE(Cruscal(3, 3, edges) == 2);
}

TEST_CASE("Test 2") {
    INFO("TEST_CASE: Test 2 started!");

    std::vector<Edge> edges = {
        Edge(1, 2, 3),
        Edge(2, 1, 3),
        Edge(2, 4, 5),
        Edge(4, 2, 5),
        Edge(1, 3, 3),
        Edge(3, 1, 3)
    };

    REQUIRE(Cruscal(4, 3, edges) == 11);
}

TEST_CASE("Test 3") {
    INFO("TEST_CASE: Test 3 started!");

    std::vector<Edge> edges = {
        Edge(1, 2, 3),
        Edge(2, 1, 3)
    };   

    REQUIRE(Cruscal(2, 1, edges) == 3);
}