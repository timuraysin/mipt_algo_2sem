/*
    Автор: Айсин Тимур Б05-927
    Описание:
    Решение задачи C.

    Решение: 
        C помощью алгоритма Косараджу найдем сильно связные компоненты графа. Сконденсируем
        граф, и в сконденсированном графе посчитаем кол-во вершин, у которых исходящая или входящая 
        степень равны 0. Тогда минимальное кол-во ребер, которое потребуется для того, чтобы сделать граф связным, 
        будет равно максимуму из этих Значений.
    Память: O(m+n), Время: O(m+n)
*/

#include <iostream>
#include <set>
#include <stack>
#include <vector>

enum TVertexColorType {
    VCT_WHITE,
    VCT_GRAY,
    VCT_BLACK
};

void VisitNodes( int currentVertice, const std::vector<std::vector<int>>& graph,
    std::vector<TVertexColorType>& verticesColor, std::stack<int>& sortedVertices )
{
    verticesColor[currentVertice] = VCT_BLACK;

    for( auto reachableVertice : graph[currentVertice] ) {
        if( verticesColor[reachableVertice] == VCT_WHITE ) {
            VisitNodes( reachableVertice, graph, verticesColor, sortedVertices );
        }
    }

    // когда выходим из элемента, добавляем его в стек    
    sortedVertices.push( currentVertice );
}

void MakeComponents( int currentVertice, const std::vector<std::vector<int>>& graph, 
    std::vector<int>& verticesComponent, const int number ) 
{
    verticesComponent[currentVertice] = number; // присваиваем вершине currentVertice компоненту с номером number

    for( auto reachableVertice : graph[currentVertice] ) {
        if( verticesComponent[reachableVertice] == -1 ) {
            MakeComponents( reachableVertice, graph, verticesComponent, number );
        }
    }
}

int main() 
{
    int numVertices = 0;
    int numEdges = 0;
    std::cin >> numVertices >> numEdges;

    std::vector<std::vector<int>> graph( numVertices + 1 );
    std::vector<std::vector<int>> graphT( numVertices + 1 ); // g_tr - транспонированный граф
    std::vector<TVertexColorType> verticesColor( numVertices + 1, VCT_WHITE );
    std::stack<int> sortedVertices;

    for( int i = 0; i < numEdges; ++i ) {
        int u = 0;
        int v = 0;

        std::cin >> u >> v;

        graph[u].push_back( v );
        graphT[v].push_back( u );
    }

    for( int i = 1; i <= numVertices; ++i ) {
        if( verticesColor[i] == VCT_WHITE ) {
            VisitNodes( i, graph, verticesColor, sortedVertices ); // в out будут лежать вершину в порядке убывания времени выхода
        }
    }

    std::vector<int> verticesComponent(numVertices + 1, -1);

    int numCondensedVertices = 1; // номер цвета, которым мы помечаем вершины на текущей итерации
    while( !sortedVertices.empty() ) {
        int currentVertice = sortedVertices.top();
        sortedVertices.pop();

        if( verticesComponent[currentVertice] != -1 ) {
            continue;
        }

        MakeComponents( currentVertice, graphT, verticesComponent, numCondensedVertices );
        numCondensedVertices++;
    }

    // Множества вершин, обладающих входными и выходными ребрами соответственно
    std::set<int> verticesWithInEdges;
    std::set<int> verticesWithOutEdges;

    for( int i = 1; i <= numVertices; ++i ) {
        for( auto x : graph[i] ) {
            if( verticesComponent[i] != verticesComponent[x] ) {
                verticesWithInEdges.insert(verticesComponent[x]);
                verticesWithOutEdges.insert(verticesComponent[i]);
            }
        }
    }

    // если кол-во сильно связных компонент - 1, то добавлять ребра не нужно
    std::cout << ( (numCondensedVertices == 2) ? 0 : std::max( (numCondensedVertices-1)-verticesWithInEdges.size(),
        (numCondensedVertices-1)-verticesWithOutEdges.size() ) ) << std::endl;

    return 0;
}